import React from 'react';  
import { useState } from 'react';
import styled from 'styled-components';


const StyledWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 642px;
    margin: 0 auto;
    border: 1px solid red;
    padding: 20px;
`;

const StyledCard = styled.button`
    background-color: #333333;
    border: 1px solid #000;
    border-radius: 15px;
    box-shadow:  none;
    width: 100px;
    height: 100px;
    outline: none;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 10px;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    
`;

// Funkcja oblicza lizby losowe od 0 do 17
function randomValue(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
    
}



// setFields(randomValue);
const wyn = randomValue(0,17);

const FIELD_VALUES = [
    'apple.jpg',
    'avocado.jpg',
    'banana.jpg',
    'billberry.jpg',
    'blackberry.jpg',
    'blueberry.jpg',
    'cherry.jpg',
    'coconut.jpg',
    'fig.jpg',
    'grapefruit.jpg',
    'grapes.jpg',  
    'kiwi.jpg',
    'lemon.jpg',
    'melon.jpg',
    'nut.jpg',
    'papaya.jpg',
    'peach.jpg',
    'pear.jpg',
];

console.log('Wybrano ' + wyn + 'o wartości ' + FIELD_VALUES[wyn]);
console.log(FIELD_VALUES);

const initialValues = [
    'apple.jpg',
    'avocado.jpg',
    'banana.jpg',
    'billberry.jpg',
    'blackberry.jpg',
    'blueberry.jpg',
    'cherry.jpg',
    'coconut.jpg',
    'fig.jpg',
    'grapefruit.jpg',
    'grapes.jpg',  
    'kiwi.jpg',
    'lemon.jpg',
    'melon.jpg',
    'nut.jpg',
    'papaya.jpg',
    'peach.jpg',
    'pear.jpg',
    'apple.jpg',
    'avocado.jpg',
    'banana.jpg',
    'billberry.jpg',
    'blackberry.jpg',
    'blueberry.jpg',
    'cherry.jpg',
    'coconut.jpg',
    'fig.jpg',
    'grapefruit.jpg',
    'grapes.jpg',  
    'kiwi.jpg',
    'lemon.jpg',
    'melon.jpg',
    'nut.jpg',
    'papaya.jpg',
    'peach.jpg',
    'pear.jpg',
];




const MemoryGame = () => {
    const [fields, setFields] = useState(initialValues);
    
    
    const checkFields = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17];
    console.log("Długość check field " + checkFields.length);
    const iteratorCheckFields = checkFields.length;
    var j=0;
    for (var i=0; i<iteratorCheckFields; i++) {
        
        console.log("długośc CheckFields length = " + checkFields.length);
        console.log("Tablica checkFields" + checkFields);
        const randomValueIndex = checkFields.indexOf(randomValue(0,checkFields.length));
        
        // if (checkFields[randomValueIndex] !== -1){
            fields[i]=checkFields[randomValueIndex];
            checkFields.splice(randomValueIndex,1);
        // };
        
        
        console.log("iteracja " + i);
        console.log("Pozycje randomvalueindex " + randomValueIndex);
    };
    
    console.log("Kolejne pola przycisków: " + fields);
    
    
    return (<>
        <h1>Gra memory 11111</h1> 
        
        <StyledWrapper>
            {fields.map((fieldValue,index) => (
                <StyledCard key={index}>
                    {/* <img src="/images/banana.jpg" alt="banana.jpg"/> */}
                    {fieldValue}
                </StyledCard>
            ))}    
        </StyledWrapper>
    </>);
}
 
export default MemoryGame;