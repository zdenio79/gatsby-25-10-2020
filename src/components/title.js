import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const StyledWrapper = styled.div`
    padding: 100px;
    background-color: ${({theme}) => theme.colors.mainGreen};
    border: 3px solid #e4e4e4;
    @media(max-width: 600px){
        padding: 30px;
    }

`;

const StyledHeader = styled.h1`
    display: flex;
    justify-content: center;
    background-color: ${({changeHeaderColor}) => changeHeaderColor ? 'aquamarine' : 'red'};
    height: ${({height}) => height}px;
    font-family:"Lato";
    font-size: 56pt;
    span {
        font-weight: bold;
        color: purple;
    }

`;

const StyledSubHeader = styled.h2`
    display: flex;
    justify-content: center;    
    background-color: green;
    color: red;
    &.bold {
        font-weight: 700;
    }
    
`;

// Rozszerzenie stylu StyledSubHeader o kolejne właściwości
const SyledHeaderWithBorder = styled(StyledSubHeader)`
    border: 5px solid pink;
`;

const Title = ({title,subtitle, changeHeaderColor}) => {
    return (<>
        <StyledWrapper>
            <StyledHeader 
                changeHeaderColor={changeHeaderColor}
                height={changeHeaderColor ? 500 : 300}
            >
                <span>1</span>{title}
            </StyledHeader>
            <StyledSubHeader>{subtitle}</StyledSubHeader>
            <StyledSubHeader>{subtitle}</StyledSubHeader>
            <SyledHeaderWithBorder className="bold"> {subtitle}</SyledHeaderWithBorder>
        </StyledWrapper>
    </>)
}


// Domyślne właściwości
Title.defaultProps = {
    title: 'Uzupełnij tytuł',
    subtitle: 'Uzupełnij podtytuł',
    changeHeaderColor: false,
}

// Zapisujemy oczekiwane wartości propsów czyli tych popbranych ze strony
Title.propTypes = {
    title: PropTypes.string,
    subtitle: PropTypes.string,
    changeHeaderColor: PropTypes.bool,
}
export default Title;