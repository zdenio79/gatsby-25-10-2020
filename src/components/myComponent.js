import React from 'react';

export class MyComponent extends React.Component {
    state = {
        iterator: 0,
    }


    clickMethod() {
        this.setState({
            iterator: this.state.iterator + 1,
        });
    }
    
    render() {
        const {title, children} = this.props;
        const { iterator } = this.state;
        return ( <>
                <h1>{ title }</h1>
                <h2>Numer: { iterator } </h2>
                <div>
                    { children }
                </div>
                <button onClick={this.clickMethod.bind(this)}>Kliknij mnie</button>
            </>
        )
    }
}
