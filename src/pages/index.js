import React from "react"
import { Link } from "gatsby"


import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Title from "../components/title"


const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi people</h1>
    <p>Witam na stronie z Gatsbym 111111. </p>
    <p>Teraz zbudujemy coś wspaniałego.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    
    <Link to="new-game">Nowa gra</Link>
    <br />  
    

    <Link to="kolko-i-krzyzyk">Kółko i Krzyżyk</Link>
    <br />  
    
    <Link to="/memory">Gra pamięciowa</Link>
    <br />
    
    <button>
      <Link to="/moja-strona/">Go to "Moja Strona"</Link>
    </button>
    <br />
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
    <Title 
      title="tytuł 2"
      subtitle="podtytuł 2"

    />
    <Title />
    <Title />
   
  </Layout>
)

export default IndexPage
