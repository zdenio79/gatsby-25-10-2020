import React from 'react';  
import Layout from '../components/layout';
import NewGame from '../components/NewGame';
import SEO from "../components/seo"
import { Link } from "gatsby"

const NewPage = () => {
    return ( 
        <Layout>
            <SEO title="Nowa gra" />
            <h1>Moja nowa gra</h1>
            <Link to="/">Wroc na strone glowna</Link>

            <NewGame />


        </Layout>
     );
}
 
export default NewPage;