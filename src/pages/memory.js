import React from 'react';  
import { Link } from "gatsby"
import Layout from '../components/layout';
import SEO from '../components/seo';
import MemoryGame from '../components/MemoryGame';

const MemoryPage = () => {
    return ( 
        <Layout>
            <SEO title="Gra pamięciowa" />
            <h1>Gra pamięciopwa</h1>
            <Link to="/">Wroc na strone glowna</Link>
            <img src="../images/banana.jpg" alt="baban"/>
            <MemoryGame />
            
        </Layout>
     );
}

export default MemoryPage;