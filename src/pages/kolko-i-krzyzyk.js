import React from 'react';  
import { Link } from 'gatsby';
import Layout from '../components/layout';
import SEO from '../components/seo';
import CircleAndCrossGame from '../components/CircleAndCrossGame'

const KolkoiKrzyzykPage = () => {
    return ( 
        <Layout>
            <SEO title="Kółko i krzyżyk" />
            <h1>Kółko i Krzyżyk</h1>
            <Link to="/">Wroc na strone glowna</Link>

            <CircleAndCrossGame />


        </Layout>
     );
}
 
export default KolkoiKrzyzykPage;