import { Link } from 'gatsby';
import React from 'react';  
import { MyComponent2 } from '../components/myComponent2';
import Layout from '../components/layout';
import SEO from '../components/seo';
import Title from '../components/title';

const arrayOfObjects = [{
    id: 1,
    title: 'Title 1',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 2,
    title: 'Title 2',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 3,
    title: 'Title 3',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}, {
    id: 4,
    title: 'Title 4',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: false,
}, {
    id: 5,
    title: 'Title 5',
    text: 'Lorem ipsum dolor sit amet',
    showAdditionalHeader: true,
}];

class MojaStronaPage extends React.Component {

    componentDidMount() {
    }

    componentWillUnmount() {
        console.log('tu component will unmount!');
    }

    render() {
        return (
            <Layout>
                <SEO title="Moja strona!s" />              
                <h1>Moja strona</h1>
                <button>
                    <Link to="/">Wroc na strone glowna</Link>
                </button>
                {arrayOfObjects.map(
                    (object, key) => (
                    <MyComponent2 key={key} {...object} />
                    )
                )}
                <Title 
                    title="jakiś fajny tytuł"
                    subtitle="Jakiś fajny pdtytuł"
                    changeHeaderColor={true}
                />
                <Title 
                    title="jakiś fajny tytuł"
                    subtitle="Jakiś fajny pdtytuł"
                />
                <Title 
                    title="dsfdsfsdfsdf"
                    subtitle="sdfsdfsdfsdfdf"
                />
            </Layout>
        );
    }    
}


export default MojaStronaPage;
 